# Language Recognition



## Introduction

In this project the user can:

1. See the percentage distribution of the recognized languages of the loaded web page. By default there are dictionaries for ten languages available : English, French, Spanish, German, Polish, Russian, Romanian, Turkish, Hungarian and Italian.
2. Add to the program a new language to recognize.
3. Teach the program to better recognize the selected language.

The program learns to recognize languages through the use of unigrams and bigrams.

## Tools

Visual Studio 2017 : C#, XAML

## Unigrams and Bigrams Algorithm

### Adding new words 
The program, when fed the text, according to the user’s selection, breaks it down into words or pairs of words as seen in the examples below:

**_Unigram option selected:_**
Hello beautiful world! -> {hello, beautiful, world}

**_Bigram option selected:_**
Hello beautiful world! -> {hello beautiful, beautiful world}

Later, thus created words/pairs are added to an existing dictionary (provided their values are unique) or, if a language is new, a new dictionary is created.

### Language recognition 
After the web page is loaded, its content gets broken down into words/pairs. Next, the created this way list is compared with the lists from all dictionaries. All occurrences of the same words/pairs are added together and then divided through the quantity of all the words/pairs from the loaded web page, giving a percentage distribution of recognized languages.

## User Interface
![userinterface](https://gitlab.com/WeronikaSendys/language-recognition/-/raw/main/images/userinterface.png)

| Command | Description |
| ------ | ------ |
|GO|Navigates to the website and calls the functions that recognize the language of the loaded web page.|
|Unigram/Bigram|Depending on the selection the program will recognize/learn languages through the use of unigrams or bigrams.|
|LEARN|Adds new words/pairs of words to a dictionary or, if the dictionary of the specified language does not exist yet, creates a new dictionary.|
|File>Load|Deletes the current dictionaries and loads new ones from a specified .zip file.|
|File>Save|Saves all dictionaries into a .zip file.|
|File>Learn from file|Learns new words/pairs of words from a specified .txt file|
|File>Clear|Deletes all dictionaries.|
|File>Exit|Exits the program.|

## The Most Important Classes

| Class | Description |
| ------ | ------ |
|RecognizeBase|The skeleton of language recognition algorithm (base class for design pattern: Template method).|
|RecognizeByUnigram,  RecognizeByBigram|Classes implementing the details of RecognizeBase.|
|StoreToFile|Implementation of program functions related to loading and saving created dictionaries for language recognition.|
|LanguageDictionary|Dictionary of words for a specific language.|
|Languages|Set of all dictionaries.|
|RecognizedResult|The result of the language recognition algorithm.|
|WebHelper|Support for extracting text from indicated web pages for further analysis.|
|RecognizeGram|The core class that supports the functionality of the program.|

## Class Diagram
![classdiagram](https://gitlab.com/WeronikaSendys/language-recognition/-/raw/main/images/classdiagram.png)

