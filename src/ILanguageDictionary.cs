﻿using System.Collections.Generic;

namespace LanguageRecognition
{
    public interface ILanguageDictionary
    {
        string Language { get; }
        IEnumerable<string> GetList { get; }
        void Add(List<string> words);
        bool Contains(string word);
        int Contains(List<string> words);
        
        void Save(ILanguageStore engine);
        
    }
}
