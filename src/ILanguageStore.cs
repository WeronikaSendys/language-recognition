﻿using System.Collections.Generic;

namespace LanguageRecognition
{
    public interface ILanguageStore
    {
        void Save(string Name, IEnumerable<string> Words);
        
    }
}
