﻿using System.Collections.Generic;

namespace LanguageRecognition
{
    public interface ILanguagesStore: ILanguageStore
    {
        IEnumerable<string> GetLanguages();
        
        IEnumerable<string> GetWords(string Name);
    }
}
