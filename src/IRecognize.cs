﻿namespace LanguageRecognition
{
    public interface IRecognize
    {
        void AddPattern(string language, string text);
        void AddFile(string language, string name);
        IRecognizedResult Recognize(string text);
        void Save(IRecognizedStore engine);
        void Load(IRecognizedStore engine);
    }
}
