﻿namespace LanguageRecognition
{
    public interface IRecognizedLanguage
    {
        string Language { get; }
        double Probability { get; }

        string ToString();
    }
}
