﻿using System.Collections.Generic;

namespace LanguageRecognition
{
    public interface IRecognizedResult
    {
        IEnumerable<IRecognizedLanguage> Languages { get; }
        string Text();
    }
}
