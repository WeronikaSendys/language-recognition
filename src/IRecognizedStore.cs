﻿namespace LanguageRecognition
{
    public interface IRecognizedStore
    {
        
        ILanguagesStore SetNGramName(string Name);
        
    }
}
