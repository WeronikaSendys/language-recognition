﻿namespace LanguageRecognition
{
    public interface IStore
    {
        IRecognizedStore Load(string Name);
        IRecognizedStore Save(string Name);
    }
}
