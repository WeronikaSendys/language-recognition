﻿namespace LanguageRecognition
{
    public interface ITextReceiver
    {
        void Add(string mes);
        void AddLine();
        void AddLine(string mes);
        void Clear();
    }
}
