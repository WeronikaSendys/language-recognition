﻿using System.Collections.Generic;
using System.Linq;

namespace LanguageRecognition
{
    public class LanguageDictionary : ILanguageDictionary
    {
        private string _language;
        private HashSet<string> _dictionary;
        public LanguageDictionary(string language) {
            _language = language;
            _dictionary = new HashSet<string>();
        }
        public LanguageDictionary(string language, IEnumerable<string> words)
        {
            _language = language;
            _dictionary = new HashSet<string>(words);
            
        }

        public string Language => _language;

        public IEnumerable<string> GetList => _dictionary;

        public void Add(List<string> words)
        {
            foreach (var w in words)
            {
                _dictionary.Add(w);
            }
        }

        public bool Contains(string word)
        {
            return _dictionary.Contains(word);
        }

        public int Contains(List<string> words)
        {
            int result = 0;
            foreach(var w in words)
            {
                if (_dictionary.Contains(w))
                {
                    ++result;
                }
            }
            return result;
        }

        

        public void Save(ILanguageStore engine)
        {
            engine.Save(_language, _dictionary.OrderBy(w => w));
      
        }
    }
}
