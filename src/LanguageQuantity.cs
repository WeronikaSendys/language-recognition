﻿using System;

namespace LanguageRecognition
{
    public class LanguageQuantity: IComparable<LanguageQuantity>
    {
        private string _language;
        private int _quantity;

        public LanguageQuantity(string language,int quantity)
        {
            _language = language;
            _quantity = quantity;
        }
        public string Language => _language;
        public int Quantity => _quantity;

        public int CompareTo(LanguageQuantity other)
        {
            if (other.Quantity > _quantity)
            {
                return 1;
            }
            if (other.Quantity < _quantity)
            {
                return -1;
            }
            return 0;
        }
    }
}
