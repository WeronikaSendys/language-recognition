﻿using System.Collections.Generic;

namespace LanguageRecognition
{
    public class Languages 
    {

        private Dictionary<string, ILanguageDictionary> _languages;
        public Languages()
        {
            _languages = new Dictionary<string, ILanguageDictionary>();
        }
        void AddLanguage(string language)
        {
            if (_languages.ContainsKey(language))
            {
                return;
            }
            _languages.Add(language, new LanguageDictionary(language));

        }
        public void AddWords(string language, List<string> words)
        {
            ILanguageDictionary dict;
            if (!_languages.TryGetValue(language, out dict))
            {
                dict = new LanguageDictionary(language);
                _languages.Add(language, dict);
            }
            dict.Add(words);

        }
        public List<LanguageQuantity> Matches(List<string> words)
        {
            var result = new List<LanguageQuantity>();

            foreach (var elem in _languages)
            {
                string Language = elem.Key;
                int Quantity = elem.Value.Contains(words);
                if (Quantity > 0)
                {
                    result.Add(new LanguageQuantity(Language, Quantity));
                }
            }

            return result;
        }
        public void Save(ILanguagesStore engine)
        {
            foreach(var elem in _languages)
            {
                elem.Value.Save(engine);
            }
            

        }
       public void Load(ILanguagesStore engine)
        {
            var elems = engine.GetLanguages();
            _languages = new Dictionary<string, ILanguageDictionary>();

            foreach (var elem in elems)
            {
                if (_languages.ContainsKey(elem))
                {
                    continue;
                }
                var words = engine.GetWords(elem);
                _languages.Add(elem, new LanguageDictionary(elem, words));
               
            }
            
        }

    }
        
}
