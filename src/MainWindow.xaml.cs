﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Navigation;

namespace LanguageRecognition
{
    public partial class MainWindow : Window
    {
        private ITextReceiver Msg;
        private RecognizeGram Recognize;
        private string LanguageName => txtLanguage.Text;
        private string PageAddress {
            get {
                return PageAddr.Text;
            }
            set {
                PageAddr.Text = value;
            }
        }
        private bool IsUnigram
        {
            get
            {
                return CheckUnigram.IsChecked ?? false;
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            Msg = new TextBlockReceiver(Result);
            Recognize = new RecognizeGram();
            try
            {
                Recognize.LoadDefault();
            }
            catch (Exception e)
            {
                MessageBox.Show("An error occurred loading a configuration file : " + e.Message);
            }


            }


        private void WEB_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            PageAddress = e.Uri.OriginalString;

            if (Msg == null)
            {
                return;
            }
            Msg.Clear();
            Msg.Add("Navigation to ");
            Msg.Add(e.Uri.OriginalString);
            Msg.AddLine();
        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void WEB_LoadCompleted(object sender, NavigationEventArgs e)
        {
            if (Msg == null)
            {
                return;
            }

            Msg.AddLine("Loaded.");

            var text = WebHelper.GetBody(WEB.Document);
            var result = Recognize.Recognize(IsUnigram,text);
            Msg.AddLine(result.Text());
          
        }

   

        private void btnGO_Click(object sender, RoutedEventArgs e)
        {
            WEB.Navigate(PageAddress);
        }

        private void btnLEARN_Click(object sender, RoutedEventArgs e)
        {
         if (String.IsNullOrWhiteSpace(LanguageName))
            {
                Msg.AddLine("Please enter the name of the language.");
                return;
            }
            var text = WebHelper.GetBody(WEB.Document);
            Recognize.AddPattern(IsUnigram,LanguageName, text);

        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            Msg.AddLine("Loading began.");
            OpenFileDialog file = new OpenFileDialog();
            file.DefaultExt = ".zip";
            file.Filter = "Zip documents (.zip)|*.zip";
            if (file.ShowDialog() == true)
            {
                Msg.AddLine("The selected file: "+ file.FileName);
                try
                {
                    Recognize.Load(file.FileName);
                    Msg.AddLine("Loading finished.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred loading the selected configuration file: " + ex.Message);
                }
            }
        }
        private void LearnFromFile_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(LanguageName))
            {
                Msg.AddLine("Please enter the language name.");
                return;
            }
            Msg.AddLine("Learing from file .txt began.");
            OpenFileDialog file = new OpenFileDialog();
            file.DefaultExt = ".txt";
            file.Filter = "Text documents (.txt)|*.txt";
            if (file.ShowDialog() == true)
            {
                Msg.AddLine("Selected file: " + file.FileName);
                try
                {
                    Recognize.AddFile(IsUnigram, LanguageName, file.FileName);
                    Msg.AddLine("Learning from file .txt finished.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred loading a file: " + ex.Message);
                }

            }
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Msg.AddLine("Saving began.");
            SaveFileDialog file = new SaveFileDialog();
            if (file.ShowDialog()==true)
            {
                Msg.AddLine("Selected file:" + file.FileName);
                try
                {
                   Recognize.Save(file.FileName);
                    Msg.AddLine("Saving finished.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred saving a file:" + ex.Message);
                }
                
            }
        }
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            Msg.AddLine("Configuration deleted.");
            Recognize.Clear();
        }
    }
}
