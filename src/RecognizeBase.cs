﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LanguageRecognition
{
    public class RecognizeBase 
    {
        private Languages _languages;
        private string _name;
        public RecognizeBase(string name)
        {
            _languages = new Languages();
            _name = name;
        }
        public void AddPattern(string language, string text)
        {
            var words = String2UniqueWords(text);
            _languages.AddWords(language, words);
        }
        public void AddFile(string language, string name)
        {
            using(var reader = new StreamReader(name,Encoding.UTF8, true))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var words = String2UniqueWords(line);
                    _languages.AddWords(language, words);
                }
            }
        }
        public IRecognizedResult Recognize(string text)
        {
            var words = String2UniqueWords(text);
            var quantity = _languages.Matches(words);
            quantity.Sort();
            return new RecognizedResult(words.Count, quantity);
        }

      
        protected virtual List<string> String2UniqueWords(string text)
        {
            throw new NotImplementedException();
        }
       public void Save(IRecognizedStore engine)
        {
            ILanguagesStore engineforlanguages =engine.SetNGramName(_name);
            _languages.Save(engineforlanguages);
        }
        public void Load(IRecognizedStore engine)
        {
            ILanguagesStore engineforlanguages = engine.SetNGramName(_name);
            _languages.Load(engineforlanguages);
        }
    }
}
