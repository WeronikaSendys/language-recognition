﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LanguageRecognition
{
    public class RecognizeByBigram : RecognizeBase,IRecognize
    {
        public RecognizeByBigram(): base("Bigram")
        {
        }
        protected override List<string> String2UniqueWords(string text)
        {

            var helper = new WordHelper();
            var words = helper.String2Word(text);
            var tmp = new List<String>();
            if (words.Count <2)
            {
                return tmp;
            }
            string first=words[0];

            for(int i = 1; i < words.Count; ++i)
            {
                tmp.Add(first + " " + words[i]);
                first = words[i];
            }
            var uniquewords = tmp.Distinct().ToList();
            return uniquewords;
        }
    }
}
