﻿using System.Collections.Generic;
using System.Linq;

namespace LanguageRecognition
{
    public class RecognizeByUnigram : RecognizeBase, IRecognize
    {
        public RecognizeByUnigram(): base("Unigram")
        {
        }
        protected override List<string> String2UniqueWords(string text)
        {
            var helper = new WordHelper();
            var words = helper.String2Word(text);
            var uniquewords = words.Distinct().ToList();
            return uniquewords;
        }
    }
}
