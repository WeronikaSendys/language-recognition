﻿using System;
using System.IO;

namespace LanguageRecognition
{
    public class RecognizeGram
    {
        readonly string DEFAULT_CONFIG = @"Config/Default.zip";
        private IRecognize _unigram;
        private IRecognize _bigram;
        
    
     
        public RecognizeGram()
        {
            _unigram = new RecognizeByUnigram();
            _bigram = new RecognizeByBigram();
        }
        public void Clear()
        {
            _unigram = new RecognizeByUnigram();
            _bigram = new RecognizeByBigram();
        }
        public void AddPattern(bool IsUnigram, string language, string text)
        {
            if (String.IsNullOrWhiteSpace(language))
            {
                return;
            }
            if (IsUnigram)
            {
                _unigram.AddPattern(language, text);
            }
            else
            {
                _bigram.AddPattern(language, text);
            }
        }
        public void AddFile(bool IsUnigram, string language, string name)
        {
            if (String.IsNullOrWhiteSpace(language))
            {
                return;
            }
            if (IsUnigram)
            {
                _unigram.AddFile(language, name);
            }
            else
            {
                _bigram.AddFile(language, name);
            }
        }
        public IRecognizedResult Recognize(bool IsUnigram, string text)
        {
            if (IsUnigram)
            {
                return _unigram.Recognize(text);
            }
            return _bigram.Recognize(text);

        }
        public void Save(string name)
        {
            using (var store = new StoreToFile())
            {
                var engine = store.Save(name);
                _unigram.Save(engine);
                _bigram.Save(engine);
            }

        }
        public void Load(string name)
        {
            using ( var store = new StoreToFile()){
                var engine = store.Load(name);
                _unigram.Load(engine);
                _bigram.Load(engine);
            }
        }
        
        public void LoadDefault()
        {
            Load(Directory.GetCurrentDirectory() + "/" + DEFAULT_CONFIG);
        }
    }
    
   
}
