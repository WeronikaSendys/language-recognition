﻿using System;

namespace LanguageRecognition
{
    public class RecognizedLanguage : IRecognizedLanguage
    {
        private string _language;
        private double _probability;
        public RecognizedLanguage(string language, double probability)
        {
            _language = language;
            _probability = probability;
        }
        public string Language => _language;

        public double Probability => _probability;

        public override string ToString()
        {
            return _language + ": " + Math.Round(_probability,3).ToString("#0.0%");
        }
    }
}
