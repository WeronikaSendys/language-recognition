﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LanguageRecognition
{
    public class RecognizedResult : IRecognizedResult
    {
        private List<IRecognizedLanguage> _results;
        public RecognizedResult(int wordsCount, IEnumerable<LanguageQuantity> languageQuantity)
        {
            _results = new List<IRecognizedLanguage>();
            if (wordsCount == 0)
            {
                return;
            }
            
            foreach (var elem in languageQuantity)
            {
                var language = elem.Language;
                double probability = Convert.ToDouble(elem.Quantity) / Convert.ToDouble(wordsCount);
                _results.Add(new RecognizedLanguage(language, probability));
            }
        }
        public IEnumerable<IRecognizedLanguage> Languages => _results;
        
        public string Text()
        {
            if (_results.Count == 0)
            {
                return "No language has been recognized.";
            }
            var result = new StringBuilder();
            
            foreach (var language in _results)
            {
                result.AppendLine(language.ToString());
            }
            return result.ToString();
        }
    }
}
