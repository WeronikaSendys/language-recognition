﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;

namespace LanguageRecognition
{
    public class StoreToFile : IStore, IDisposable, IRecognizedStore, ILanguagesStore
    {
        
        private ZipArchive _zip;
        private string _path;

        public void Dispose()
        {
            if (_zip == null)
            {
                return;
            }
            _zip.Dispose();
            _zip = null;
            _path = null;
            GC.SuppressFinalize(this);
        }

        public IRecognizedStore Load(string Name)
        {
            _zip = ZipFile.OpenRead(Name);
            return this;
        }

        public IRecognizedStore Save(string Name)
        {
            var tmp = Name.EndsWith(".zip") ? Name : Name + ".zip";
            _zip = ZipFile.Open(tmp, ZipArchiveMode.Create);
            return this;
        }

        IEnumerable<string> ILanguagesStore.GetLanguages()
        {
            List<string> result = new List<string>();
            var beginPath = _path + "/";
            foreach (var elem in _zip.Entries)
            {
                if (elem.FullName.StartsWith(beginPath))
                {
                    result.Add(elem.Name.Substring(0,elem.Name.Length-4));
                }
            }
            return result;
        }

        IEnumerable<string> ILanguagesStore.GetWords(string Name)
        {
            List<string> result=new List<string>();
            var entryName = GetEntryName(Name);
            var entry = _zip.GetEntry(GetEntryName(Name));
            using (StreamReader reader = new StreamReader(entry.Open()))
            {
                while (!reader.EndOfStream)
                {
                    var word = reader.ReadLine();
                    result.Add(word);
                }
            }
            return result;
        }
        ILanguagesStore IRecognizedStore.SetNGramName(string Name)
        {
            _path = Name;
            return this;
        }
        void ILanguageStore.Save(string Name, IEnumerable<string> Words)
        {
            ZipArchiveEntry entry = _zip.CreateEntry(GetEntryName(Name));
            using (StreamWriter stream = new StreamWriter(entry.Open()))
            {
                foreach(var word in Words)
                {
                    stream.WriteLine(word);
                }
            }
        }
        private String GetEntryName(string Name)
        {
            return _path + "/" + Name + ".txt";
        }
    }
}
