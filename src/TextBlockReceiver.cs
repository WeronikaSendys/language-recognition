﻿using System;
using System.Windows.Controls;

namespace LanguageRecognition
{
    public class TextBlockReceiver : ITextReceiver
    {
        private TextBlock _box;
   
        public TextBlockReceiver(TextBlock box)
        {
            _box = box;
        }
        public void Add(string mes)
        {
            _box.Text += mes;
        }



        public void AddLine()
        {
            _box.Text += "\n";
        }

        public void AddLine(string mes)
        {
            Add(mes);
            AddLine();
        }

        public void Clear()
        {
            _box.Text = String.Empty;
        }
    }
}
