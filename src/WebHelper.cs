﻿using mshtml;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace LanguageRecognition
{
    public static class WebHelper
    {
        public static string ReadTextFromUrl(string url)
        {
            using (var client = new WebClient())
            {
                using (var stream = client.OpenRead(url))
                {
                    using (var textReader = new StreamReader(stream, Encoding.UTF8, true))
                    {
                        return textReader.ReadToEnd();
                    }
                }
            }
        }

        public static string GetBody(string content)
        {
            var xmldoc = new XmlDocument();
            xmldoc.PreserveWhitespace = false;
            var s = "<!DOCTYPE html>" + content.Substring(15);
            

         xmldoc.LoadXml(s);
            
            var body =  xmldoc.GetElementsByTagName("body");
            return body[0].InnerText;
        }
        public static string GetBody(object doc)
        {
            string result = string.Empty;
            var htmldoc = doc as HTMLDocument;
            var body = htmldoc.body;
            var bodyelems = body.all;
            
            result = htmldoc.body.innerText;
            return result;
        }

    }

}
