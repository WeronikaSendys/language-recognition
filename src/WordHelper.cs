﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace LanguageRecognition
{
    public class WordHelper
    {
        public List<string> String2Word(string str)
        {
            var result = new List<string>();
            var strt = str.Split(new char[] { ' ', '.', ',', ';', ':', '?', '-','\n','_' }, 
                                 StringSplitOptions.RemoveEmptyEntries);
            foreach (var s in strt)
            {
                if (!IsLengthAccepted(s))
                {
                    continue;
                }
                var sc = Clean(s);
                if (!IsLengthAccepted(sc))
                {
                    continue;
                }
                result.Add(sc.ToLower());
            }
            return result;
        }
        private bool IsLengthAccepted(string s)
        {
            return (s.Length > 2 && s.Length < 21);
        }
        private string Clean(string s)
        {
            return Regex.Replace(s, @"[\W\d]",string.Empty,RegexOptions.None);
        }

    }
}
